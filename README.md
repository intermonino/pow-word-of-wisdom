# WORD OF WISDOM
## Условие
- TCP server should be protected from DDOS attacks with the [Prof of Work](https://en.wikipedia.org/wiki/Proof_of_work), the challenge-response protocol should be used.  
- The choice of the POW algorithm should be explained.  
- After Prof Of Work verification, server should send one of the quotes from “word of wisdom” book or any other collection of the quotes.  
- Docker file should be provided both for the server and for the client that solves the POW challenge
- Наличие Юнит-тестов;
- Наличие Readme;

## Запуск
### Локально без Docker:
При запуске берутся переменные окружения из ОС.
Если их не задать - возьмутся дефолтно заданные в config.go.

#### Запуск сервера:
```shell
make run_server
```

#### Запуск клиента:
```shell
make run_client
```

При успешной инициализации соединения и решения загадки, пользователь сможет передавать сообщения на сервер из консоли.

### Локально с docker-compose:
При запуске берутся переменные окружения из файла `.env`.

#### Запуск клиента и сервера :
```shell
make compose
```

## Об реализации
В качестве транспортного протокола, согласно ТЗ взят протокол [TCP](https://ru.wikipedia.org/wiki/Transmission_Control_Protocol).

В качетсве сериализатора использовался [gob](https://pkg.go.dev/encoding/gob)

В качестве алгоритма PoW был взят алгоритм основанный на Merkle Tree. Последовательность работы клиента-сервера заключается в следующем:
1. Клиент устанавливает соединение с сервером.
2. Сервер посылает приветственное сообщение.
3. Каждому новому клиенту, а так же клиенту, который не проходил валидацию в течение `CACHE_TTL`, необходимо запросить загадку.
4. В качестве загадки клиенту необходимо построить Merkle Tree, на основе массива из Payload присылаемого сервером в ответ на запрос загадки.
5. Сервер валидирует наличие контента в присланном ему дереве. Контент для каждого клента заведомо сохранен в кэше.
6. В случае успешной валидации - серевер начинает "бизнесовую" обработку последующих TCP-запросов от клиента.

Самописная реализация Merkle Tree покрыта Unit Тестами с использованием `тест-suite` из библиотеки [testify](https://github.com/stretchr/testify).

## Переменные окружения

| Название    | тип           | default | Описание
|-------------|---------------|---------|--------------------------------------
| SERVER_ADDR | string        | :8765   | Адресс и порт на котором слушает сервер
| CACHE_TTL   | time.Duration | 2h      | Время жизни сессии клиента
| TIMEOUT     | time.Duration | 30s     | Время ожидания при чтении
| REQ_BUFFER_SIZE     | int           | 1048576 | Размер буфера
| VERIFICATION_COUNT     | int           | 1       | Кол-во верификаций Payload
| IS_DOCKER     | bool          | false   | Если true - отрабатывает автоматическая генерация сообщений. Если false - ручной ввод сообщений из stdout

## Merkle Tree

![img.png](img.png)

