package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/rs/zerolog"

	"word_of_wisdom/internal/client"
	"word_of_wisdom/internal/config"
)

func main() {
	// Инициализация окружения
	logger := zerolog.New(os.Stdout)
	conf, err := config.InitFromEnv()
	if err != nil {
		logger.Fatal().Err(err).Msg("Init config error")
	}
	c := client.New(conf, logger)
	ctx, cancel := context.WithCancel(context.Background())
	go initGracefulShutdown(cancel)
	c.Run(ctx)
}

func initGracefulShutdown(cancelFunc context.CancelFunc) {
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-done
	cancelFunc()
}
