package main

import (
	"context"
	"github.com/rs/zerolog"
	"os"
	"os/signal"
	"syscall"
	"word_of_wisdom/internal/config"
	"word_of_wisdom/internal/server"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	logger := zerolog.New(os.Stdout)
	conf, err := config.InitFromEnv()
	if err != nil {
		logger.Fatal().Err(err).Msg("Init config error")
	}
	tcpServ := server.New(conf, logger.With().Str("module", "tcp_server").Logger())
	go initGracefulShutdown(cancel, tcpServ)
	tcpServ.Listen(ctx)
	logger.Info().Msg("Server finished")
}

func initGracefulShutdown(cancelFunc context.CancelFunc, tcpServ *server.POW_TCPListener) {
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-done
	cancelFunc()
	tcpServ.Close()
}
