FROM golang:alpine as build

RUN apk add git gcc

WORKDIR /build

COPY go.mod go.sum ./
RUN  go mod download

COPY cmd/server      cmd/server
COPY internal/    internal/

RUN cd /build/cmd/server && \
    go build -o /bin/server

FROM alpine:latest
LABEL maintainer="Antony Martynov <intermonino@gmail.com> tg:@ntnmrtnv"
LABEL service="TCP Server"

COPY --from=build /bin /bin

WORKDIR /bin
CMD /bin/server