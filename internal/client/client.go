package client

import (
	"bufio"
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/gob"
	"errors"
	"net"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"word_of_wisdom/internal/config"
	"word_of_wisdom/internal/merkle"
	"word_of_wisdom/internal/server"
	"word_of_wisdom/internal/utils"
)

type Client struct {
	serverAddr string
	bufSize    int64
	timeout    time.Duration
	isDocker   bool

	l *zerolog.Logger
}

func New(conf *config.Config, l zerolog.Logger) *Client {
	return &Client{
		serverAddr: conf.ServerAddr,
		bufSize:    conf.BufferSize,
		isDocker:   conf.IsDocker,
		timeout:    conf.Timeout,
		l:          &l,
	}
}

func (c *Client) Run(ctx context.Context) {
	// инициализация подключения
	conn, err := net.Dial("tcp", c.serverAddr)
	if err != nil {
		c.l.Fatal().Err(err).Msg("Dial conn error")
	}
	defer conn.Close()

	msg, err := server.ReadMessage(conn, c.timeout)
	if err != nil {
		c.l.Fatal().Err(err).Msg("Read first msg error")
	}

	c.l.Info().Msg(msg.Msg)

	// Решение пазла
	err = c.GetAndSolvePuzzle(conn)
	if err != nil {
		c.l.Fatal().Err(err).Msg("GetAndSolvePuzzle error")
	}

	// Обработка сообщений
	for {
		select {
		case <-ctx.Done():
			c.l.Info().Msg("Client gracefully stopped")
			return
		default:
			text := ""
			if c.isDocker {
				// Генерация случайной строки
				text = utils.RandStringRunes(10)
				c.l.Info().Msg("Sending generated text:" + text)
			} else {
				// Чтение входных данных от stdin
				reader := bufio.NewReader(os.Stdin)
				c.l.Info().Msg("Text to send: ")
				text, _ = reader.ReadString('\n')
			}

			err = server.SendMessage(conn, text)
			if err != nil {
				c.l.Error().Err(err).Msg("Sending msg error")
				continue
			}

			// Прослушиваем ответ
			msg, err = server.ReadMessage(conn, c.timeout)
			if err != nil {
				c.l.Error().Err(err).Msg("Read msg error")
			}
			c.l.Info().Msg(msg.Msg)
		}

	}

}

func (c *Client) GetAndSolvePuzzle(conn net.Conn) error {
	// инициализация буфера ответов
	respBuff := make([]byte, c.bufSize)
	respObj := gob.NewDecoder(bytes.NewBuffer(respBuff))

	// инициализация буфера запросов
	reqBuff := new(bytes.Buffer)
	reqObj := gob.NewEncoder(reqBuff)

	// Запрос пазла
	err := reqObj.Encode(&server.GetPuzzleRequest{PayloadType: merkle.SHA256PayloadType})
	if err != nil {
		c.l.Error().Err(err).Msg("GetPuzzleRequest encode decoding error")
		return err
	}
	_, err = conn.Write(reqBuff.Bytes())
	if err != nil {
		c.l.Error().Err(err).Msg("GetPuzzleRequest writing error")
		return err
	}
	reqBuff.Reset()

	// получение пазла
	err = conn.SetReadDeadline(time.Now().Add(c.timeout))
	if err != nil {
		c.l.Error().Err(err).Msg("SetReadDeadline error")
		return err
	}
	_, err = conn.Read(respBuff)
	if err != nil {
		c.l.Error().Err(err).Msg("GetPuzzleResponse reading error")
		return err
	}

	resp := &server.GetPuzzleResponse{}
	err = respObj.Decode(resp)
	if err != nil {
		c.l.Error().Err(err).Msg("GetPuzzleResponse decoding error")
		return err
	}
	log.Info().Interface("data_set", resp.DataArray).Msg("incoming payload")

	// сборка дерева и его отправка
	tree, err := merkle.NewTree(resp.DataArray, sha256.New)
	if err != nil {
		c.l.Error().Err(err).Msg("Tree generation error")
		return err
	}
	tree.HashType = merkle.SHA256PayloadType

	err = reqObj.Encode(&server.VerifyRequest{Tree: tree})
	if err != nil {
		c.l.Error().Err(err).Msg("VerifyRequest encode error")
		return err
	}
	_, err = conn.Write(reqBuff.Bytes())
	if err != nil {
		c.l.Error().Err(err).Msg("VerifyRequest encode error")
		return err
	}

	respBuff = make([]byte, c.bufSize)
	respObj = gob.NewDecoder(bytes.NewBuffer(respBuff))
	err = conn.SetReadDeadline(time.Now().Add(c.timeout))
	if err != nil {
		c.l.Error().Err(err).Msg("SetReadDeadline error")
		return err
	}
	_, err = conn.Read(respBuff)
	if err != nil {
		c.l.Error().Err(err).Msg("VerifyResponse reading error")
		return err
	}

	resp2 := &server.VerifyResponse{}
	err = respObj.Decode(resp2)
	if err != nil {
		c.l.Error().Err(err).Msg("VerifyResponse decoding error")
		return err
	}

	c.l.Info().Bool("result", resp2.Success).Msg("verify result")
	if !resp2.Success {
		c.l.Error().Msg("unsuccessful verify")
		err = errors.New("unsuccessful verify")
		return err
	}
	return nil
}
