package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	CacheTTL          time.Duration `envconfig:"CACHE_TTL" default:"2h"`
	PayloadsCount     int64         `envconfig:"PAYLOADS_COUNT" default:"32"`
	BufferSize        int64         `envconfig:"REQ_BUFFER_SIZE" default:"1048576"`
	VerificationCount int64         `envconfig:"VERIFICATION_COUNT" default:"3"`
	ServerAddr        string        `envconfig:"SERVER_ADDR" default:":8765"`
	IsDocker          bool          `envconfig:"IS_DOCKER" default:"false"`
	Timeout           time.Duration `envconfig:"TIMEOUT" default:"30s"`
}

func InitFromEnv() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("", c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
