package merkle

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/gob"
	"errors"
	"hash"
)

var ErrEmptySet = errors.New("error: cannot construct tree without payload")

func init() {
	gob.Register(Tree{})
}

// Tree - реализация merkle tree
type Tree struct {
	Root  *Node   `json:"root"`
	leafs []*Node `json:"-"`

	// HashFabric - фабрика хешей
	HashFabric func() hash.Hash
	HashType   int
}

// NewTree - создает MerkleTree с заданным методом хэширования.
// Если hashMethod == nil, тогда по умолчанию используется sha256
func NewTree(cs []Payload, hashFabric func() hash.Hash) (*Tree, error) {
	if hashFabric == nil {
		hashFabric = sha256.New
	}
	t := &Tree{
		HashFabric: hashFabric,
	}
	err := t.build(cs)
	if err != nil {
		return nil, err
	}
	return t, nil
}

func (t *Tree) build(dataSet []Payload) error {

	// Проверка на пустоту входного множетсва данных
	if len(dataSet) == 0 {
		return ErrEmptySet
	}

	// Получение первых листовых нод
	leafNodes := make([]*Node, 0, len(dataSet))
	for _, data := range dataSet {
		hashValue, err := data.Hash()
		if err != nil {
			return err
		}

		leafNodes = append(leafNodes, &Node{
			IsLeaf: true,
			Hash:   hashValue,
			Data:   data,
		})
	}

	// Если кол-во нод нечетное - добовляем дубликат из последнего листового узла
	if len(leafNodes)%2 == 1 {
		duplicateNode := *leafNodes[len(leafNodes)-1]
		duplicateNode.IsDuplicate = true
		leafNodes = append(leafNodes, &duplicateNode)
	}

	// Сборка дерева
	root, err := t.recursiveBuildTreeBody(leafNodes)
	if err != nil {
		return err
	}

	t.Root = root
	t.leafs = leafNodes
	return nil
}

func (t *Tree) recursiveBuildTreeBody(children []*Node) (*Node, error) {
	parents := make([]*Node, 0, len(children)/2)
	for i := 0; i < len(children); i += 2 {
		left := i
		right := left + 1

		// защита от паник
		if i+1 == len(children) {
			right = i
		}

		// сборка ноды родителя
		p := &Node{
			Left:  children[left],
			Right: children[right],
		}
		children[left].parent = p
		children[right].parent = p

		hashValue, err := p.nodeHash(t.HashFabric)
		if err != nil {
			return nil, err
		}
		p.Hash = hashValue

		parents = append(parents, p)
	}

	// Если дошли до корня
	if len(children) == 2 {
		return parents[0], nil
	}

	return t.recursiveBuildTreeBody(parents)
}

func (t *Tree) GetAllPayloads() []Payload {
	res := make([]Payload, 0, len(t.leafs))
	for _, l := range t.leafs {
		res = append(res, l.Data)
	}
	return res
}

func (t *Tree) Restore() {
	t.Root.RestoreParent()
	t.leafs = t.Root.GetLeafs()
	switch t.HashType {
	case SHA1PayloadType:
		t.HashFabric = sha1.New
	case MD5PayloadType:
		t.HashFabric = md5.New
	default:
		t.HashFabric = sha256.New
	}
}
