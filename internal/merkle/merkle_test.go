package merkle

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"github.com/stretchr/testify/suite"
	"hash"
	"reflect"
	"runtime"
	"testing"
)

type treeTestCase struct {
	Id             int
	Name           string
	HashFabric     func() hash.Hash
	HashMethodName string
	DataSet        []Payload

	ExpectedRootHash   []byte
	ExpectedLeafsCount int
	ExpectedError      error
}

type treeSuite struct {
	suite.Suite
	testCases []*treeTestCase
}

func (suite *treeSuite) SetupTest() {
	suite.testCases = []*treeTestCase{
		{
			Id:             1,
			Name:           "Success case",
			HashFabric:     sha256.New,
			HashMethodName: "sha256",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
				SHA256Payload{
					Foo: "Hola",
				},
			},
			ExpectedRootHash: []byte{0x25, 0x8c, 0xbf, 0x47, 0x5f, 0x64, 0x23, 0x3b, 0xe2, 0xff, 0x69, 0x77, 0x77, 0x94,
				0xb1, 0xcd, 0x55, 0xe5, 0x3f, 0xf8, 0xf9, 0xbf, 0xdd, 0x4e, 0x81, 0x74, 0xf4, 0x5f, 0x13, 0xb9, 0x47, 0x98},
			ExpectedLeafsCount: 4,
		},
		{
			Id:             2,
			Name:           "Default hash factory",
			HashMethodName: "default",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
				SHA256Payload{
					Foo: "Hola",
				},
			},
			ExpectedRootHash: []byte{0x25, 0x8c, 0xbf, 0x47, 0x5f, 0x64, 0x23, 0x3b, 0xe2, 0xff, 0x69, 0x77, 0x77, 0x94,
				0xb1, 0xcd, 0x55, 0xe5, 0x3f, 0xf8, 0xf9, 0xbf, 0xdd, 0x4e, 0x81, 0x74, 0xf4, 0x5f, 0x13, 0xb9, 0x47, 0x98},
			ExpectedLeafsCount: 4,
		},
		{
			Id:             3,
			Name:           "Duplicate generation",
			HashFabric:     sha256.New,
			HashMethodName: "sha256",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
			},
			ExpectedRootHash: []byte{0x9e, 0x6b, 0x2a, 0x2c, 0xbe, 0x64, 0xe0, 0xd3, 0x16, 0x10, 0xc7, 0x3b, 0x10, 0x84,
				0x88, 0x3, 0x98, 0x26, 0x62, 0x24, 0xc4, 0x8, 0x94, 0xa3, 0x4f, 0xe2, 0x52, 0x56, 0x9b, 0x61, 0x8e, 0x5d},
			ExpectedLeafsCount: 4,
		},
		{
			Id:             4,
			Name:           "SHA1 hash method",
			HashFabric:     sha1.New,
			HashMethodName: "sha1",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
			},
			ExpectedRootHash: []byte{0x7c, 0x72, 0xda, 0x5c, 0xbd, 0x40, 0x2e, 0x16, 0xde, 0xa2, 0x8, 0x50, 0x4a, 0x22,
				0x5c, 0xfb, 0xe5, 0x54, 0xa, 0x1e},
			ExpectedLeafsCount: 4,
		},
		{
			Id:             5,
			Name:           "MD5 hash method",
			HashFabric:     md5.New,
			HashMethodName: "md5",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
			},
			ExpectedRootHash:   []byte{0x3c, 0xc1, 0x8e, 0x1f, 0xf7, 0x26, 0x51, 0x6d, 0xfc, 0x97, 0x96, 0x86, 0xeb, 0x53, 0xef, 0x40},
			ExpectedLeafsCount: 4,
		},
		{
			Id:                 6,
			Name:               "empty set error",
			HashFabric:         md5.New,
			HashMethodName:     "nil",
			DataSet:            []Payload{},
			ExpectedRootHash:   nil,
			ExpectedLeafsCount: 0,
			ExpectedError:      ErrEmptySet,
		},
	}
}

func (suite *treeSuite) TestBuildingTree() {
	for i := 0; i < len(suite.testCases); i++ {
		testCase := suite.testCases[i]
		tree, err := NewTree(testCase.DataSet, testCase.HashFabric)
		suite.Equalf(testCase.ExpectedError, err, "|case:%d| error: %v", testCase.Id, err)
		if err != nil {
			suite.Nil(tree, "|case:%d| tree isn't nil", testCase.Id)
		}
		if tree != nil {
			act := runtime.FuncForPC(reflect.ValueOf(tree.HashFabric).Pointer()).Name()
			if testCase.HashFabric != nil {
				exp := runtime.FuncForPC(reflect.ValueOf(testCase.HashFabric).Pointer()).Name()
				suite.Equalf(exp, act, "|case:%d| error: %v", testCase.Id, err)
			}
			switch testCase.HashMethodName {
			case "md5":
				exp := runtime.FuncForPC(reflect.ValueOf(md5.New).Pointer()).Name()
				suite.Equalf(exp, act, "|case:%d| invalid hash method", testCase.Id)
			case "sha1":
				exp := runtime.FuncForPC(reflect.ValueOf(sha1.New).Pointer()).Name()
				suite.Equalf(exp, act, "|case:%d| invalid hash method", testCase.Id)
			default:
				exp := runtime.FuncForPC(reflect.ValueOf(sha256.New).Pointer()).Name()
				suite.Equalf(exp, act, "|case:%d| invalid hash method", testCase.Id)
			}
			suite.Equalf(testCase.ExpectedLeafsCount, len(tree.leafs), "|case:%d| invalid hash method", testCase.Id)
			suite.Equalf(testCase.ExpectedRootHash, tree.Root.Hash, "|case:%d| invalid root hash", testCase.Id)
		}
	}
}

func TestTree_NewTree(t *testing.T) {
	suite.Run(t, new(treeSuite))
}
