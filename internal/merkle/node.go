package merkle

import (
	"bytes"
	"encoding/gob"
	"hash"
)

func init() {
	gob.Register(Node{})
}

//Node - реализует ноды дерева. В частном случае может быть листом.
type Node struct {
	// Расположение нод соседних нод
	parent *Node `json:"-"`
	Left   *Node `json:"left"`
	Right  *Node `json:"right"`

	// Hash - хэш ноды
	Hash []byte `json:"hash"`

	// Вспомогательные поля
	Data        Payload `json:"data"`
	IsLeaf      bool    `json:"is-leaf"`
	IsDuplicate bool    `json:"is-duplicate"`
}

func (n *Node) nodeHash(hashFabric func() hash.Hash) ([]byte, error) {
	if n.IsLeaf {
		return n.Data.Hash()
	}
	h := hashFabric()
	concat := append(n.Left.Hash, n.Right.Hash...)
	if _, err := h.Write(concat); err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}

func (n *Node) Verify(hashFabric func() hash.Hash) ([]byte, bool, error) {
	if n.IsLeaf {
		hashValue, err := n.Data.Hash()
		return hashValue, true, err
	}

	rightBytes, isValid, err := n.Right.Verify(hashFabric)
	if err != nil {
		return nil, false, err
	}
	if !isValid {
		return nil, false, nil
	}

	leftBytes, isValid, err := n.Left.Verify(hashFabric)
	if err != nil {
		return nil, false, err
	}
	if !isValid {
		return nil, false, nil
	}

	h := hashFabric()
	if _, err := h.Write(append(leftBytes, rightBytes...)); err != nil {
		return nil, false, err
	}

	calculatedValue := h.Sum(nil)
	if bytes.Compare(n.Hash, calculatedValue) != 0 {
		return nil, false, nil
	}

	return calculatedValue, true, nil
}

func (n *Node) IsEqualData(data Payload) bool {
	if !n.IsLeaf {
		return false
	}
	return n.Data.IsEqual(data)
}

func (n *Node) RestoreParent() {
	if n.IsLeaf {
		return
	}
	n.Right.parent = n
	n.Left.parent = n
	n.Right.RestoreParent()
	n.Left.RestoreParent()
	return
}

func (n *Node) GetLeafs() []*Node {
	var res []*Node
	if n.IsLeaf {
		return []*Node{n}
	}
	res = append(res, n.Left.GetLeafs()...)
	res = append(res, n.Right.GetLeafs()...)
	return res
}
