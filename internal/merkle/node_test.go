package merkle

import (
	"crypto/sha256"
	"testing"

	"github.com/stretchr/testify/suite"
)

type nodeTestCase struct {
	Id                       int
	Name                     string
	TestNode                 *Node
	ExpectedLeafCount        int
	ExpectedEqualData        Payload
	ExpectedValidationResult bool
	ExpectedTestNodeHash     []byte
}

type nodeSuite struct {
	suite.Suite
	testCases []*nodeTestCase
}

func (suite *nodeSuite) SetupTest() {
	suite.testCases = []*nodeTestCase{
		{
			Id:   1,
			Name: "Success case",
			TestNode: &Node{
				parent: nil,
				Left: &Node{
					parent:      nil,
					Data:        SHA256Payload{Foo: "mi first msg"},
					IsLeaf:      true,
					IsDuplicate: false,
				},
				Right: &Node{
					parent:      nil,
					Data:        SHA256Payload{Foo: "some hash"},
					IsLeaf:      true,
					IsDuplicate: false,
				},
				Hash:        nil,
				Data:        nil,
				IsLeaf:      false,
				IsDuplicate: false,
			},
			ExpectedEqualData:        SHA256Payload{Foo: "mi first msg"},
			ExpectedLeafCount:        2,
			ExpectedValidationResult: true,
			ExpectedTestNodeHash: []byte{0x2c, 0xf5, 0xf0, 0x21, 0x56, 0xf8, 0xbd, 0xb6, 0xd1, 0x9f, 0xc, 0xd4, 0xb8,
				0x2f, 0xb0, 0x39, 0x36, 0xa9, 0xd0, 0x94, 0x7f, 0xf6, 0xc2, 0x5f, 0x82, 0x36, 0x99, 0xb0, 0xee, 0x7a, 0x2c, 0x95},
		},
	}
}

func (suite *nodeSuite) TestNodes() {
	for i := 0; i < len(suite.testCases); i++ {
		testCase := suite.testCases[i]
		leafs := testCase.TestNode.GetLeafs()
		suite.Equalf(testCase.ExpectedLeafCount, len(leafs), "|case:%d| unexpected leaf count", testCase.Id)

		suite.Equalf(false, testCase.TestNode.IsEqualData(testCase.ExpectedEqualData), "|case:%d| unexpected comparing payload results")
		suite.Equalf(true, leafs[0].IsEqualData(testCase.ExpectedEqualData), "|case:%d| unexpected comparing payload results")

		testCase.TestNode.RestoreParent()

		suite.Equalf(testCase.TestNode, testCase.TestNode.Left.parent, "|case:%d| unexpected parent")
		suite.Equalf(testCase.TestNode, testCase.TestNode.Right.parent, "|case:%d| unexpected parent")

		var err error
		testCase.TestNode.Left.Hash, err = testCase.TestNode.Left.Data.Hash()
		suite.NoErrorf(err, "|case:%d| unexpected error", testCase.Id)

		testCase.TestNode.Right.Hash, err = testCase.TestNode.Right.Data.Hash()
		suite.NoErrorf(err, "|case:%d| unexpected error", testCase.Id)

		testCase.TestNode.Hash, err = testCase.TestNode.nodeHash(sha256.New)
		suite.NoErrorf(err, "|case:%d| unexpected error", testCase.Id)

		hash, isValid, err := testCase.TestNode.Verify(sha256.New)
		suite.NoErrorf(err, "|case:%d| unexpected error", testCase.Id)
		suite.Equalf(testCase.ExpectedValidationResult, isValid, "|case:%d| unexpected validation node result", testCase.Id)
		suite.Equalf(testCase.ExpectedTestNodeHash, hash, "|case:%d| unexpected validation node hash", testCase.Id)

	}
}

func TestNode_Node(t *testing.T) {
	suite.Run(t, new(nodeSuite))
}
