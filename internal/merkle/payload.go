package merkle

import (
	"crypto/sha256"
	"encoding/gob"

	"word_of_wisdom/internal/utils"
)

const (
	SHA256PayloadType = iota
	SHA1PayloadType
	MD5PayloadType
)

func init() {
	gob.Register(SHA256Payload{})
	gob.Register(SHA1Payload{})
	gob.Register(MD5Payload{})
}

// Payload - интерфейс, для полезной нагрузки хранимой в дереве.
type Payload interface {
	IsEqual(data Payload) bool
	Hash() ([]byte, error)
}

// SHA256 Payload

type SHA256Payload struct {
	Foo string
}

func (t SHA256Payload) Hash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(t.Foo)); err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

func (t SHA256Payload) IsEqual(data Payload) bool {
	return t.Foo == data.(SHA256Payload).Foo
}

// SHA1 Payload

type SHA1Payload struct {
	Foo string `json:"foo"`
}

func (t SHA1Payload) Hash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(t.Foo)); err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

func (t SHA1Payload) IsEqual(data Payload) bool {
	return t.Foo == data.(SHA1Payload).Foo
}

// MD5 Payload

type MD5Payload struct {
	Foo string
}

func (t MD5Payload) Hash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(t.Foo)); err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

func (t MD5Payload) IsEqual(data Payload) bool {
	return t.Foo == data.(MD5Payload).Foo
}

func GeneratePayloads(count, hashType int64) []Payload {
	res := make([]Payload, 0, count)
	for i := int64(0); i < count; i++ {
		switch hashType {
		case SHA1PayloadType:
			res = append(res, SHA1Payload{Foo: utils.RandStringRunes(10)})
		case MD5PayloadType:
			res = append(res, MD5Payload{Foo: utils.RandStringRunes(10)})
		default:
			res = append(res, SHA256Payload{Foo: utils.RandStringRunes(10)})
		}
	}
	return res
}
