package merkle

import "bytes"

// Verify - проверка корректности дерева. Рекурсивный спуск от корня к листьям и обратный подъем
// в рамках которго происходит рехеширование детей и сверка полученного хеша с родительским хешом.
func (t *Tree) Verify() (bool, error) {
	_, isValid, err := t.Root.Verify(t.HashFabric)
	if err != nil {
		return false, err
	}

	return isValid, nil
}

// VerifyPayload - проверка на наличие Payload в дереве.
// Для этой проверки не нужно строить все дерево заново - достаточно иметь хеши необходимые для построения
// пути от листа до корня.
func (t *Tree) VerifyPayload(data Payload) (bool, error) {
	for _, l := range t.leafs {
		if l.Data.IsEqual(data) {
			p := l.parent
			for p != nil {
				h := t.HashFabric()
				if _, err := h.Write(append(p.Left.Hash, p.Right.Hash...)); err != nil {
					return false, err
				}
				if bytes.Compare(h.Sum(nil), p.Hash) != 0 {
					return false, nil
				}
				p = p.parent
			}
			return true, nil
		}
	}
	return false, nil
}
