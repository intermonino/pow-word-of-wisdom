package merkle

import (
	"crypto/sha256"
	"github.com/stretchr/testify/suite"
	"hash"
	"testing"
)

type powTestCase struct {
	Id             int
	Name           string
	HashFabric     func() hash.Hash
	HashMethodName string
	DataSet        []Payload
	DataForVerify  Payload

	ExpectedDataVerificationResult bool
	ReplacedRootHash               []byte
	ExpectedTreeVerificationResult bool
}

type powSuite struct {
	suite.Suite
	testCases []*powTestCase
}

func (suite *powSuite) SetupTest() {
	suite.testCases = []*powTestCase{
		{
			Id:             1,
			Name:           "Success case",
			HashFabric:     sha256.New,
			HashMethodName: "sha256",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
				SHA256Payload{
					Foo: "Hola",
				},
			},
			ReplacedRootHash: []byte{0x25, 0x8c, 0xbf, 0x47, 0x5f, 0x64, 0x23, 0x3b, 0xe2, 0xff, 0x69, 0x77, 0x77, 0x94,
				0xb1, 0xcd, 0x55, 0xe5, 0x3f, 0xf8, 0xf9, 0xbf, 0xdd, 0x4e, 0x81, 0x74, 0xf4, 0x5f, 0x13, 0xb9, 0x47, 0x98},
			DataForVerify: SHA256Payload{
				Foo: "Hola",
			},
			ExpectedDataVerificationResult: true,
			ExpectedTreeVerificationResult: true,
		},
		{
			Id:             2,
			Name:           "Bad payload verification",
			HashFabric:     sha256.New,
			HashMethodName: "sha256",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
				SHA256Payload{
					Foo: "Hola",
				},
			},
			ReplacedRootHash: []byte{0x25, 0x8c, 0xbf, 0x47, 0x5f, 0x64, 0x23, 0x3b, 0xe2, 0xff, 0x69, 0x77, 0x77, 0x94,
				0xb1, 0xcd, 0x55, 0xe5, 0x3f, 0xf8, 0xf9, 0xbf, 0xdd, 0x4e, 0x81, 0x74, 0xf4, 0x5f, 0x13, 0xb9, 0x47, 0x98},
			DataForVerify: SHA256Payload{
				Foo: "Hello",
			},
			ExpectedDataVerificationResult: false,
			ExpectedTreeVerificationResult: true,
		},
		{
			Id:             3,
			Name:           "Bad tree verification",
			HashFabric:     sha256.New,
			HashMethodName: "sha256",
			DataSet: []Payload{
				SHA256Payload{
					Foo: "Salam",
				},
				SHA256Payload{
					Foo: "Hi",
				},
				SHA256Payload{
					Foo: "Privet",
				},
				SHA256Payload{
					Foo: "Hola",
				},
			},
			ReplacedRootHash: []byte{0x25},
			DataForVerify: SHA256Payload{
				Foo: "Hello",
			},
			ExpectedDataVerificationResult: false,
			ExpectedTreeVerificationResult: false,
		},
	}
}

func (suite *powSuite) TestVerifyData() {
	for i := 0; i < len(suite.testCases); i++ {
		testCase := suite.testCases[i]
		tree, err := NewTree(testCase.DataSet, testCase.HashFabric)
		suite.Equalf(nil, err, "|case:%d| error: %v", testCase.Id, err)

		isValid, err := tree.VerifyPayload(testCase.DataForVerify)
		suite.Equalf(nil, err, "|case:%d| error: %v", testCase.Id, err)
		suite.Equal(testCase.ExpectedDataVerificationResult, isValid, "|case:%d| verification tree is failed")
	}
}

func (suite *powSuite) TestVerify() {
	for i := 0; i < len(suite.testCases); i++ {
		testCase := suite.testCases[i]
		tree, err := NewTree(testCase.DataSet, testCase.HashFabric)
		suite.Equalf(nil, err, "|case:%d| error: %v", testCase.Id, err)

		// replace root hash
		tree.Root.Hash = testCase.ReplacedRootHash

		isValid, err := tree.Verify()
		suite.Equalf(nil, err, "|case:%d| error: %v", testCase.Id, err)
		suite.Equal(testCase.ExpectedTreeVerificationResult, isValid, "|case:%d| verification tree is failed")
	}
}

func TestTree_VerifyPayload(t *testing.T) {
	suite.Run(t, new(powSuite))
}
