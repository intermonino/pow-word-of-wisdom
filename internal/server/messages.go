package server

import (
	"encoding/gob"

	"word_of_wisdom/internal/merkle"
)

func init() {
	gob.Register(GetPuzzleRequest{})
	gob.Register(GetPuzzleResponse{})
	gob.Register(VerifyRequest{})
	gob.Register(VerifyResponse{})
	gob.Register(Message{})
}

type GetPuzzleRequest struct {
	PayloadType int64 `json:"payload-type"`
}

type GetPuzzleResponse struct {
	DataArray []merkle.Payload `json:"data-array"`
}

type VerifyRequest struct {
	Tree *merkle.Tree `json:"tree"`
}

type VerifyResponse struct {
	Msg     string
	Success bool `json:"success"`
}

type Message struct {
	Msg string `json:"msg"`
}
