package server

import (
	"bytes"
	"context"
	"encoding/gob"
	"errors"
	"net"
	"sync"
	"time"

	"github.com/rs/zerolog"

	"word_of_wisdom/internal/config"
	"word_of_wisdom/internal/merkle"

	"word_of_wisdom/internal/timemark_storage"
)

type POW_TCPListener struct {
	ln *net.TCPListener

	availableCons timemark_storage.TimeMarkStorage

	payloadCheckCount int64
	bufferSize        int64
	verificationCount int64
	timeout           time.Duration

	logger *zerolog.Logger
	wg     *sync.WaitGroup
}

func New(conf *config.Config, logger zerolog.Logger) *POW_TCPListener {
	tcpAddr, err := net.ResolveTCPAddr("tcp", conf.ServerAddr)
	if err != nil {
		logger.Fatal().Err(err).Msg("Can't resolve tcp")
		return nil
	}
	ln, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		logger.Fatal().Err(err).Msg("Can't listen tcp addr")
		return nil
	}

	return &POW_TCPListener{
		ln:                ln,
		availableCons:     timemark_storage.NewDummy(conf.CacheTTL),
		payloadCheckCount: conf.PayloadsCount,
		bufferSize:        conf.BufferSize,
		verificationCount: conf.VerificationCount,
		logger:            &logger,
		wg:                &sync.WaitGroup{},
		timeout:           conf.Timeout,
	}
}

func (p *POW_TCPListener) GeneratePuzzle(addr string, hashType int64) []merkle.Payload {
	payloads := merkle.GeneratePayloads(p.payloadCheckCount, hashType)
	p.availableCons.SetDataSet(addr, arrayToSet(payloads))
	return payloads
}

func arrayToSet(payloads []merkle.Payload) map[merkle.Payload]struct{} {
	payloadsSet := make(map[merkle.Payload]struct{}, len(payloads))
	for _, p := range payloads {
		payloadsSet[p] = struct{}{}
	}
	return payloadsSet
}

func (p *POW_TCPListener) Listen(ctx context.Context) {
	p.logger.Info().Msg("Server ready to listen")
	for {
		select {
		case <-ctx.Done():
			p.wg.Wait()
			return
		default:
			conn, err := p.ln.AcceptTCP()
			if err != nil {
				if errors.Is(err, net.ErrClosed) {
					return
				}
				p.logger.Error().Err(err).Msg("accept error")
				continue
			}
			err = SendMessage(conn, "Hi!")
			if err != nil {
				p.logger.Error().Err(err).Msg("Sending message error")
				return
			}
			p.wg.Add(1)
			go p.handleConnection(ctx, conn)
		}
	}
}

func (p *POW_TCPListener) handleConnection(ctx context.Context, conn net.Conn) {
	reqAddr := conn.RemoteAddr().String()
	l := p.logger.With().Str("method", "handleConnection").Str("req_addr", reqAddr).Logger()

	defer func() {
		l.Info().Msg("Stop handling connection")
		p.wg.Done()
		conn.Close()
	}()

	for {
		select {
		case <-ctx.Done():
			return
		default:
			p.logger.Info().Str("addr", reqAddr).Msg("Serving")
			// если конекшн еще есть в хранилище - значит можно обрабатывать дальше
			if !p.availableCons.IsAlive(reqAddr) {
				// проверка клиента
				err := p.Check(conn, reqAddr)
				if err != nil {
					l.Error().Err(err).Msg("Check error")
					return
				}
				continue
			}

			// входящий запрос
			msg, err := ReadMessage(conn, p.timeout)
			if err != nil {
				l.Error().Err(err).Msg("Reading message error")
				return
			}

			l.Info().Msg(msg.Msg)

			// Ответ
			err = SendMessage(conn, "word of wisdom")
			if err != nil {
				l.Error().Err(err).Msg("Sending message error")
				return
			}
		}
	}
}

func (p *POW_TCPListener) Check(conn net.Conn, reqAddr string) error {
	l := p.logger.With().Str("method", "Check").Str("req_addr", reqAddr).Logger()
	reqBuff := make([]byte, p.bufferSize)

	err := conn.SetReadDeadline(time.Now().Add(p.timeout))
	if err != nil {
		return err
	}
	_, err = conn.Read(reqBuff)
	if err != nil {
		l.Error().Err(err).Str("method", "Check").Msg("Reading request error")
		return err
	}

	// входящий запрос
	reqObj := gob.NewDecoder(bytes.NewBuffer(reqBuff))
	req2Obj := gob.NewDecoder(bytes.NewBuffer(reqBuff))

	// ответ
	respBuffs := new(bytes.Buffer)
	respObj := gob.NewEncoder(respBuffs)

	defer func() {
		_, err := conn.Write(respBuffs.Bytes())
		if err != nil {
			l.Error().Err(err).Msg("sending msg error")
		}
	}()
	// Запрос на получение пазла
	gpr := &GetPuzzleRequest{}
	err = reqObj.Decode(gpr)
	// Если нет ошибки - отравляем ответ
	if nil == err {
		err = respObj.Encode(&GetPuzzleResponse{DataArray: p.GeneratePuzzle(reqAddr, gpr.PayloadType)})
		if err != nil {
			l.Error().Err(err).Msg("GetPuzzleResponse encoding error")
			return err
		}

		return nil
	}

	// Если возникла ошибка при декодировании в GetPuzzleRequest
	// попытка декодировать в VerifyRequest
	gvr := &VerifyRequest{}
	err = req2Obj.Decode(gvr)
	if nil == err {
		// Восстановление полученного дерева.
		gvr.Tree.Restore()

		// верификация payload в дереве
		storedSet := p.availableCons.GetDataSet(reqAddr)
		i := int64(0)
		for randData, _ := range storedSet {
			if i >= p.payloadCheckCount {
				break
			}
			isValid, err := gvr.Tree.VerifyPayload(randData)
			if err != nil {
				p.logger.Error().Err(err).Msg("VerifyPayload error")
				err = respObj.Encode(&VerifyResponse{Success: false})
				if err != nil {
					l.Error().Err(err).Msg("VerifyResponse encoding error")
					return err
				}
				p.availableCons.SetValid(reqAddr, false)
				return err
			}
			if !isValid {
				p.logger.Error().Err(err).Interface("payload", randData).Msg("Invalid payload")
				err = respObj.Encode(&VerifyResponse{Success: false})
				if err != nil {
					l.Error().Err(err).Msg("VerifyResponse encoding error")
					return err
				}
				p.availableCons.SetValid(reqAddr, false)
				return errors.New("invalid payload")
			}
		}
		p.availableCons.SetValid(reqAddr, true)
		err = respObj.Encode(&VerifyResponse{Success: true, Msg: "q"})
		if err != nil {
			p.logger.Error().Err(err).Msg("VerifyResponse encoding error")
			return err
		}
	}
	if err != nil {
		err = respObj.Encode(&Message{Msg: "Please, generate new puzzle and solve it!"})
		if err != nil {
			p.logger.Error().Err(err).Msg("GetPuzzleResponse encoding error")
			return err
		}
	}
	return nil
}

func SendMessage(conn net.Conn, msg string) error {
	respBuffs := new(bytes.Buffer)
	respObj := gob.NewEncoder(respBuffs)
	err := respObj.Encode(&Message{Msg: msg})
	if err != nil {
		return err
	}
	_, err = conn.Write(respBuffs.Bytes())
	if err != nil {
		return err
	}
	return nil
}

func ReadMessage(conn net.Conn, timeout time.Duration) (*Message, error) {
	err := conn.SetReadDeadline(time.Now().Add(timeout))
	if err != nil {
		return nil, err
	}

	reqBuff := make([]byte, 1024*1024)
	_, err = conn.Read(reqBuff)
	if err != nil {
		return nil, err
	}

	msg := &Message{}
	reqObj := gob.NewDecoder(bytes.NewBuffer(reqBuff))
	err = reqObj.Decode(msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}

func (p *POW_TCPListener) Close() {
	p.ln.Close()
}
