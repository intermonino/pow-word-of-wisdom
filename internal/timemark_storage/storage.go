package timemark_storage

import (
	"sync"
	"time"

	"word_of_wisdom/internal/merkle"
)

type TimeMarkStorage interface {
	SetDataSet(key string, dataSet map[merkle.Payload]struct{})

	IsAlive(key string) bool

	GetDataSet(key string) map[merkle.Payload]struct{}

	SetValid(key string, value bool)
}

type DummyTimeMarkStorage struct {
	mtx  sync.RWMutex
	data map[string]*cacheEntity
	ttl  time.Duration
}

type cacheEntity struct {
	DataSet map[merkle.Payload]struct{}
	Time    time.Time
	IsValid bool
}

func NewDummy(ttl time.Duration) *DummyTimeMarkStorage {
	return &DummyTimeMarkStorage{
		mtx:  sync.RWMutex{},
		data: make(map[string]*cacheEntity),
		ttl:  ttl,
	}
}

func (s *DummyTimeMarkStorage) SetDataSet(key string, dataSet map[merkle.Payload]struct{}) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	s.data[key] = &cacheEntity{
		DataSet: dataSet,
		IsValid: false,
	}
}

func (s *DummyTimeMarkStorage) IsAlive(key string) bool {
	s.mtx.RLock()
	defer s.mtx.RUnlock()
	if s.data == nil {
		return false
	}
	v, ok := s.data[key]
	if !ok {
		return false
	}
	now := time.Now()
	return now.Sub(v.Time) < s.ttl && v.IsValid
}

func (s *DummyTimeMarkStorage) GetDataSet(key string) map[merkle.Payload]struct{} {
	s.mtx.RLock()
	defer s.mtx.RUnlock()
	if s.data == nil {
		return nil
	}
	v, ok := s.data[key]
	if !ok {
		return nil
	}
	return v.DataSet
}

func (s *DummyTimeMarkStorage) SetValid(key string, value bool) {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	s.data[key].IsValid = value
	s.data[key].Time = time.Now()
}
