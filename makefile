CUR_DIR = $(shell pwd)

.PHONY: tidy
tidy:
	go mod tidy

.PHONY: run_server
run_server:
	go run ./cmd/server/main.go

.PHONY: run_client
run_server:
	go run ./cmd/client/main.go

.PHONY: compose_up
run_server:
	docker-compose up -d

.PHONY: test
test:
	go test -cover -race -v ./...